<?php
/**
 * @package     Mautic
 * @copyright   2017 Mautic Contributors. All rights reserved.
 * @author      Mautic
 * @link        http://mautic.org
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace Mautic\Migrations;

use Mautic\CoreBundle\Doctrine\AbstractMauticMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Migrations\SkipMigrationException;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170131131937 extends AbstractMauticMigration
{
    /**
     * @param Schema $schema
     *
     * @throws SkipMigrationException
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    public function preUp(Schema $schema)
    {
//        $shouldRunMigration = false; // Please modify to your needs

//        if (!$shouldRunMigration) {

 //           throw new SkipMigrationException('Schema includes this migration');
//        }
        if ($schema->hasTable($this->prefix.'deliveryprofiles')) {
            throw new SkipMigrationException('Schema includes this migration');
        }
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $sql = <<<SQL
CREATE TABLE {$this->prefix}deliveryprofiles (
  id INT(11) AUTO_INCREMENT NOT NULL,
  mailer_from_name VARCHAR(40) NOT NULL,
  mailer_from_email VARCHAR(40) NOT NULL,
  mailer_return_path VARCHAR(40),
  mailer_is_owner BOOL NOT NULL,
  mail_service VARCHAR(15)NOT NULL,
  mailer_user_name VARCHAR(20) NOT NULL,
  mailer_user_password VARCHAR(20) NOT NULL,
  mailer_handle_method VARCHAR(20) NOT NULL,
  schedule_name VARCHAR(20) NOT NULL,
  schedule_sun_start TIME,
  schedule_sun_stop TIME,
  schedule_mon_start TIME,
  schedule_mon_stop TIME,
  schedule_tue_start TIME,
  schedule_tue_stop TIME,
  schedule_wed_start TIME,
  schedule_wed_stop TIME,
  schedule_thr_start TIME,
  schedule_thr_stop TIME,
  schedule_fri_start TIME,
  schedule_fri_stop TIME,
  schedule_sat_start TIME,
  schedule_sat_stop TIME,
  delay_enable BOOL NOT NULL,
  delay_duration INT,
  missing_variable_enable BOOL NOT NULL,
  missing_notify_email VARCHAR(30),
 
  PRIMARY KEY( id )
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
SQL;
        $this->addSql($sql);
    }
}
