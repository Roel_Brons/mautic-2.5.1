<?php

namespace Mautic\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170213030111 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function preUp(Schema $schema)
    {
        if ($schema->hasTable($this->prefix.'deliveryprofiles')) {
            throw new SkipMigrationException('Schema includes this migration');
        }
    }

    public function up(Schema $schema)
    {
        $sql = <<<SQL
CREATE TABLE {$this->prefix}deliveryprofiles (
  id INT(11) AUTO_INCREMENT NOT NULL,
  is_published Boolean NOT NULL,
  date_added   DATETIME,
  created_by   INT(11),
  created_by_user VARCHAR(255),
  date_modified    DATETIME,
  modified_by   INT(11),
  modified_by_user VARCHAR(255),
  checked_out   DATETIME,
  checked_out_by INT(11),
  checked_out_by_user VARCHAR(255),
  title          VARCHAR(255) NOT NULL,
  color          VARCHAR(7),
  bundle         VARCHAR(50),
  mailer_from_name VARCHAR(40) NOT NULL,
  mailer_from_email VARCHAR(40) NOT NULL,
  mailer_return_path VARCHAR(40),
  mailer_is_owner Boolean NOT NULL,
  mailer_transport VARCHAR(50)NOT NULL,
  mailer_user VARCHAR(40),
  mailer_password VARCHAR(64),
  mail_handle_method VARCHAR(40) NOT NULL,
  schedule_name VARCHAR(50) NOT NULL,
  schedule_list VARCHAR(255),
  delay_is_enable Boolean NOT NULL,
  delay_time INT(11),
  missing_is_enable Boolean NOT NULL,
  missing_notify_email VARCHAR(40),
 
  PRIMARY KEY( id )
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
SQL;
        $this->addSql($sql);
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
