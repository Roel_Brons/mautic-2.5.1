<?php

/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace Mautic\DeliveryProfileBundle;

/**
 * Class CategoryBundle
 * Events available for CategoryBundle.
 */
final class DeliveryProfileEvents
{
    /**
     * The mautic.category_pre_save event is thrown right before a category is persisted.
     *
     * The event listener receives a
     * Mautic\CategoryBundle\Event\DeliveryProfileEvent instance.
     *
     * @var string
     */
    const DELIVERY_PRE_SAVE = 'mautic.deliveryprofile_pre_save';

    /**
     * The mautic.category_post_save event is thrown right after a category is persisted.
     *
     * The event listener receives a
     * Mautic\CategoryBundle\Event\DeliveryProfileEvent instance.
     *
     * @var string
     */
    const DELIVERYPROFILE_POST_SAVE = 'mautic.deliveryprofile_post_save';

    /**
     * The mautic.category_pre_delete event is thrown prior to when a category is deleted.
     *
     * The event listener receives a
     * Mautic\CategoryBundle\Event\DeliveryProfileEvent instance.
     *
     * @var string
     */
    const DELIVERY_PRE_DELETE = 'mautic.deliveryprofile_pre_delete';

    /**
     * The mautic.category_post_delete event is thrown after a category is deleted.
     *
     * The event listener receives a
     * Mautic\CategoryBundle\Event\DeliveryProfileEvent instance.
     *
     * @var string
     */
    const DELIVERY_POST_DELETE = 'mautic.deliveryprofile_post_delete';

    /**
     * The mautic.category_on_bundle_list_build event is thrown when a list of bundles supporting categories is build.
     *
     * The event listener receives a
     * Mautic\CategoryBundle\Event\DeliveryProfileTypesEvent instance.
     *
     * @var string
     */
    const DELIVERY_ON_BUILD_LIST_BUILD = 'mautic.deliveryprofile_on_bundle_list_build';
}
