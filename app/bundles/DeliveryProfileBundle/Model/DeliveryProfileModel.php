<?php

/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace Mautic\DeliveryProfileBundle\Model;

use Mautic\CoreBundle\Model\FormModel;
use Mautic\DeliveryProfileBundle\DeliveryProfileEvents;
use Mautic\DeliveryProfileBundle\Entity\DeliveryProfile;
use Mautic\DeliveryProfileBundle\Event\DeliveryProfileEvent;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

/**
 * Class DeliveryProfileModel
 * {@inheritdoc}
 */
class DeliveryProfileModel extends FormModel
{
    /**
     * @var null|\Symfony\Component\HttpFoundation\Request
     */
    protected $request;

    /**
     * DeliveryProfileModel constructor.
     *
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->request = $requestStack->getCurrentRequest();
    }

    public function getRepository()
    {
        return $this->em->getRepository('MauticDeliveryProfileBundle:DeliveryProfile');
    }

    public function getNameGetter()
    {
        return 'getTitle';
    }

    public function getPermissionBase($bundle = null)
    {
        if (null === $bundle) {
            $bundle = $this->request->get('bundle');
        }

        if ('global' === $bundle || empty($bundle)) {
            $bundle = 'deliveryprofile';
        }

        return $bundle.':deliveryprofiles';
    }

    /**
     * {@inheritdoc}
     *
     * @param   $entity
     * @param   $unlock
     *
     * @return mixed
     */
    public function saveEntity($entity, $unlock = true)
    {
        $startval1 = $entity->getStart1();
        $startval2 = $entity->getStart2();
        $startval3 = $entity->getStart3();
        $startval4 = $entity->getStart4();
        $startval5 = $entity->getStart5();
        $startval6 = $entity->getStart6();
        $startval7 = $entity->getStart7();

        $stopval1 = $entity->getStop1();
        $stopval2 = $entity->getStop2();
        $stopval3 = $entity->getStop3();
        $stopval4 = $entity->getStop4();
        $stopval5 = $entity->getStop5();
        $stopval6 = $entity->getStop6();
        $stopval7 = $entity->getStop7();

        $schedule_list = [
            '0' => ['1' => $startval1, '2' => $stopval1],
            '1' => ['1' => $startval2, '2' => $stopval2],
            '2' => ['1' => $startval3, '2' => $stopval3],
            '3' => ['1' => $startval4, '2' => $stopval4],
            '4' => ['1' => $startval5, '2' => $stopval5],
            '5' => ['1' => $startval6, '2' => $stopval6],
            '6' => ['1' => $startval7, '2' => $stopval7],
        ];

        $val = serialize($schedule_list);
        $entity->setScheduleList($val);

        if (empty($entity->getDelayIsEnable())) {
            $entity->setDelayIsEnable(true);
        }
        if (empty($entity->getMissingIsEnable())) {
            $entity->setMissingIsEnable(true);
        }

        //$entity->setSchedules();
        if (!$entity instanceof DeliveryProfile) {
            throw new MethodNotAllowedHttpException(['DeliveryProfile'], 'Entity must be of class DeliveryProfile()');
        }

        parent::saveEntity($entity, $unlock);
    }

    /**
     * {@inheritdoc}
     *
     * @param       $entity
     * @param       $formFactory
     * @param null  $action
     * @param array $options
     *
     * @return mixed
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function createForm($entity, $formFactory, $action = null, $options = [])
    {
        //echo "this is a test message";
        //die('d');
        if (!$entity instanceof DeliveryProfile) {
            throw new MethodNotAllowedHttpException(['DeliveryProfile']);
        }
        if (!empty($action)) {
            $options['action'] = $action;
        }

        return $formFactory->create('deliveryprofile_form', $entity, $options);
    }

    /**
     * Get a specific entity or generate a new one if id is empty.
     *
     * @param $id
     *
     * @return Category
     */
    public function getEntity($id = null)
    {
        if ($id === null) {
            return new DeliveryProfile();
        }

        $entity = parent::getEntity($id);

        return $entity;
    }

    /**
     * {@inheritdoc}
     *
     * @param $action
     * @param $event
     * @param $entity
     * @param $isNew
     *
     * @throws \Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException
     */
    protected function dispatchEvent($action, &$entity, $isNew = false, Event $event = null)
    {
        if (!$entity instanceof DeliveryProfile) {
            throw new MethodNotAllowedHttpException(['DeliveryProfile']);
        }

        switch ($action) {
            case 'pre_save':
                $name = DeliveryProfileEvents::DELIVERY_PRE_SAVE;
                break;
            case 'post_save':
                $name = DeliveryProfileEvents::DELIVERYPROFILE_POST_SAVE;
                break;
            case 'pre_delete':
                $name = DeliveryProfileEvents::DELIVERY_PRE_DELETE;
                break;
            case 'post_delete':
                $name = DeliveryProfileEvents::DELIVERY_POST_DELETE;
                break;
            default:
                return null;
        }

        if ($this->dispatcher->hasListeners($name)) {
            if (empty($event)) {
                $event = new DeliveryProfileEvent($entity, $isNew);
                $event->setEntityManager($this->em);
            }

            $this->dispatcher->dispatch($name, $event);

            return $event;
        } else {
            return null;
        }
    }

    /**
     * Get list of entities for autopopulate fields.
     *
     * @param $bundle
     * @param $filter
     * @param $limit
     *
     * @return array
     */
    public function getLookupResults($bundle, $filter = '', $limit = 10)
    {
        static $results = [];

        $key = $bundle.$filter.$limit;
        if (!isset($results[$key])) {
            $results[$key] = $this->getRepository()->getDeliveryProfileList($bundle, $filter, $limit, 0);
        }

        return $results[$key];
    }
}
