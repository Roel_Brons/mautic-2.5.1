<?php

/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

return [
    'routes' => [
        'main' => [
            'mautic_deliveryprofile_index' => [
                'path'       => '/deliveryprofiles/{bundle}/{page}',
                'controller' => 'MauticDeliveryProfileBundle:DeliveryProfile:index',
                'defaults'   => [
                    'bundle' => 'deliveryprofile',
                ],
            ],
            'mautic_deliveryprofile_action' => [
                'path'       => '/deliveryprofiles/{bundle}/{objectAction}/{objectId}',
                'controller' => 'MauticDeliveryProfileBundle:DeliveryProfile:executeDeliveryProfile',
                'defaults'   => [
                    'bundle' => 'deliveryprofile',
                ],
            ],
        ],
    ],

    'menu' => [

        'admin' => [

            'priority' => 60,
            'items'    => [

                'mautic.deliveryprofile.menu.index' => [
                    'route'     => 'mautic_deliveryprofile_index',
                    'access'    => 'deliveryprofile:deliveryprofiles:view',
                    'iconClass' => 'fa-folder',
                    'id'        => 'mautic_deliveryprofile_index',
                ],
            ],
        ],
    ],

    'services' => [
        'events' => [
            'mautic.deliveryprofile.subscriber' => [
                'class'     => 'Mautic\DeliveryProfileBundle\EventListener\DeliveryProfileSubscriber',
                'arguments' => [
                    'mautic.helper.bundle',
                    'mautic.helper.ip_lookup',
                    'mautic.core.model.auditlog',
                ],
            ],
        ],
        'forms' => [
            'mautic.form.type.deliveryprofile' => [
                'class'     => 'Mautic\DeliveryProfileBundle\Form\Type\DeliveryProfileListType',
                'arguments' => [
                    'doctrine.orm.entity_manager',
                    'translator',
                    'mautic.deliveryprofile.model.deliveryprofile',
                    'router',
                ],
                'alias' => 'deliveryprofile',
            ],
            'mautic.form.type.deliveryprofile_form' => [
                'class'     => 'Mautic\DeliveryProfileBundle\Form\Type\DeliveryProfileType',
                'alias'     => 'deliveryprofile_form',
                'arguments' => [
                    'translator',
                    'session',
                ],
            ],
            'mautic.form.type.deliveryprofile_bundles_form' => [
                'class'     => 'Mautic\DeliveryProfileBundle\Form\Type\DeliveryProfileBundlesType',
                'arguments' => [
                    'event_dispatcher',
                ],
                'alias' => 'deliveryprofile_bundles_form',
            ],
        ],
        'models' => [
            'mautic.deliveryprofile.model.deliveryprofile' => [
                'class'     => 'Mautic\DeliveryProfileBundle\Model\DeliveryProfileModel',
                'arguments' => [
                    'request_stack',
                ],
            ],
        ],
    ],
];
