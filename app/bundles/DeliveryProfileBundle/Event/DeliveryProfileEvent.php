<?php

/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace Mautic\DeliveryProfileBundle\Event;

use Mautic\CoreBundle\Event\CommonEvent;
use Mautic\DeliveryProfileBundle\Entity\DeliveryProfile;

/**
 * Class DeliveryProfileEvent.
 */
class DeliveryProfileEvent extends CommonEvent
{
    /**
     * @param Category $category
     * @param bool     $isNew
     */
    public function __construct(DeliveryProfile &$deliveryprofile, $isNew = false)
    {
        $this->entity = &$deliveryprofile;
        $this->isNew  = $isNew;
    }

    /**
     * Returns the Category entity.
     *
     * @return Category
     */
    public function getDeliveryProfile()
    {
        return $this->entity;
    }

    /**
     * Sets the Category entity.
     *
     * @param Category $category
     */
    public function setDeliveryProfile(DeliveryProfile $deliveryprofile)
    {
        $this->entity = $deliveryprofile;
    }
}
