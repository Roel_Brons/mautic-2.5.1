<?php
/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
?>

<?php //echo $view['form']->form($form, ['attr' => ['data-hide-loadingbar' => 'true']]);?>
<?php
    $view->extend('MauticCoreBundle:Default:content.html.php');
    $view['slots']->set('mauticContent', 'deliveryprofile');

    $deliveryId = $form->vars['data']->getId();
    if (!empty($deliveryId)) {
        $name   = $form->vars['data']->getMailerFromName();
        $header = $view['translator']->trans('mautic.deliveryprofile.header.edit', ['%name%' => $name]);
    } else {
        $header = $view['translator']->trans('mautic.deliveryprofile.header.new');
    }
    $view['slots']->set('headerTitle', $header);
?>
<!-- start: box layout -->
<div class="box-layout">
    <!-- container -->
    <?php echo $view['form']->start($form); ?>
    <div class="col-md-9 bg-auto height-auto bdr-r">
		<div class="pa-md">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"> Delivery Profile Settings </h3>
                </div>
                <div class="panel-body">
        			<div class="form-group mb-0">
        			    <div class="row">
                            <div class="col-sm-4<?php echo (count($form['title']->vars['errors'])) ? ' has-error' : ''; ?>">
                            	<label class="control-label mb-xs"><?php echo $view['form']->label($form['title']); ?></label>
        			            <?php echo $view['form']->widget($form['title'], ['attr' => ['placeholder' => $form['title']->vars['label']]]); ?>
                                <?php echo $view['form']->errors($form['title']); ?>
        			        </div>
                            <div class="col-sm-4<?php echo (count($form['color']->vars['errors'])) ? ' has-error' : ''; ?>">
                                <label class="control-label mb-xs"><?php echo $view['form']->label($form['color']); ?></label>
        			            <?php echo $view['form']->widget($form['color']); ?>
                                <?php echo $view['form']->errors($form['color']); ?>
        			        </div>
        			        <div class="col-sm-4<?php echo (count($form['isPublished']->vars['errors'])) ? ' has-error' : ''; ?>">
                                <label class="control-label mb-xs"><?php echo $view['form']->label($form['isPublished']); ?></label>
        			            <?php echo $view['form']->widget($form['isPublished']); ?>
                                <?php echo $view['form']->errors($form['isPublished']); ?>
        			        </div>
        			    </div>
        			</div>
                </div>
            </div>
			<hr class="mnr-md mnl-md">

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"> Mail Send Settings </h3>
                </div>
                <div class="panel-body">
    			<div class="form-group mb-0">
    			    <div class="row">
    			    	 <div class="col-sm-6<?php echo (count($form['mailer_from_name']->vars['errors'])) ? ' has-error' : ''; ?>">
    				    	<label class="control-label mb-xs"><?php echo $view['form']->label($form['mailer_from_name']); ?></label>
    			            <?php echo $view['form']->widget($form['mailer_from_name'], ['attr' => ['placeholder' => $form['mailer_from_name']->vars['label']]]); ?>
                            <?php echo $view['form']->errors($form['mailer_from_name']); ?>
    			        </div>
    			        <div class="col-sm-6<?php echo (count($form['mailer_from_email']->vars['errors'])) ? ' has-error' : ''; ?>">
                            <label class="control-label mb-xs"><?php echo $view['form']->label($form['mailer_from_email']); ?></label>
                            <?php echo $view['form']->widget($form['mailer_from_email'], ['attr' => ['placeholder' => $form['mailer_from_email']->vars['label']]]); ?>
                            <?php echo $view['form']->errors($form['mailer_from_email']); ?>
                        </div>
    			    </div>
                    <br>
    			    <div class="row">
    			    	<div class="col-sm-6<?php echo (count($form['mailer_return_path']->vars['errors'])) ? ' has-error' : ''; ?>">
    			        	<label class="control-label mb-xs"><?php echo $view['form']->label($form['mailer_return_path']); ?></label>
    			            <?php echo $view['form']->widget($form['mailer_return_path'], ['attr' => ['placeholder' => $form['mailer_return_path']->vars['label']]]); ?>
                            <?php echo $view['form']->errors($form['mailer_return_path']); ?>
    			        </div>
    			        <div class="col-sm-6<?php echo (count($form['mailer_is_owner']->vars['errors'])) ? ' has-error' : ''; ?>">
                            <label class="control-label mb-xs"><?php echo $view['form']->label($form['mailer_is_owner']); ?></label>
    			            <?php echo $view['form']->widget($form['mailer_is_owner']); ?>
                            <?php echo $view['form']->errors($form['mailer_is_owner']); ?>
    			        </div>
    			    </div>
                    <hr class="text-muted" />
    			    <div class="row">
    			    	<div class="col-sm-6<?php echo (count($form['mailer_transport']->vars['errors'])) ? ' has-error' : ''; ?>">
    			        	<label class="control-label mb-xs"><?php echo $view['form']->label($form['mailer_transport']); ?></label>
    			            <?php echo $view['form']->widget($form['mailer_transport'], ['attr' => ['placeholder' => $form['mailer_transport']->vars['label']]]); ?>
                            <?php echo $view['form']->errors($form['mailer_transport']); ?>
    			        </div>
                        <div class="col-sm-6 ">
                            <br><br>
                            <?php echo $view['form']->widget($form['mailer_test_connection_button']); ?>
                            <?php echo $view['form']->widget($form['mailer_test_send_button']); ?>
                        </div>
    			    </div>
                    <br><br>
    			    <div class="row">
    			    	<div class="col-sm-6<?php echo (count($form['mailer_user']->vars['errors'])) ? ' has-error' : ''; ?>">
    			        	<label class="control-label mb-xs"><?php echo $view['form']->label($form['mailer_user']); ?></label>
    			            <?php echo $view['form']->widget($form['mailer_user'], ['attr' => ['placeholder' => $form['mailer_user']->vars['label']]]); ?>
                            <?php echo $view['form']->errors($form['mailer_user']); ?>
    			        </div>
    			        <div class="col-sm-6<?php echo (count($form['mailer_password']->vars['errors'])) ? ' has-error' : ''; ?>">
    			        	<label class="control-label mb-xs"><?php echo $view['form']->label($form['mailer_password']); ?></label>
    			            <?php echo $view['form']->widget($form['mailer_password'], ['attr' => ['placeholder' => $form['mailer_password']->vars['label']]]); ?>
                            <?php echo $view['form']->errors($form['mailer_password']); ?>
    			        </div>
    			    </div>
                    <hr class="text-muted" />
    			    <div class="row">
    			    	<div class="col-sm-6<?php echo (count($form['mail_handle_method']->vars['errors'])) ? ' has-error' : ''; ?>">
    			        	<label class="control-label mb-xs"><?php echo $view['form']->label($form['mail_handle_method']); ?></label>
    			            <?php echo $view['form']->widget($form['mail_handle_method'], ['attr' => ['placeholder' => $form['mail_handle_method']->vars['label']]]); ?>
                            <?php echo $view['form']->errors($form['mail_handle_method']); ?>
    			        </div>
    			    </div>
    			</div>
                </div>
            </div>
			<hr class="mnr-md mnl-md">

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"> Delivery Schedule </h3>
                </div>
                <div class="panel-body">
                <div class="form-group mb-0">
                    <div class="row">
                        <div class="col-sm-6<?php echo (count($form['schedule_name']->vars['errors'])) ? ' has-error' : ''; ?>">
                            <label class="control-label mb-xs"><?php echo $view['form']->label($form['schedule_name']); ?></label>
                            <?php echo $view['form']->widget($form['schedule_name'], ['attr' => ['placeholder' => $form['schedule_name']->vars['label']]]); ?>
                            <?php echo $view['form']->errors($form['schedule_name']); ?>
                        </div>
                    </div>
                    <br>

                    <?php // schedule list arrangement?>
                    <div class="row">
                        <div class="col-sm-2 ">
                            <?php echo $view->render(
                                'MauticCoreBundle:Helper:publishstatus_icon.html.php',
                                ['item' => $item, 'model' => 'deliveryprofile', 'query' => 'bundle='.$bundle]
                            ); ?>
                            <span><?php echo 'Sunday'; ?></span>    
                            <br><br>    
                        </div>
                        <div class="col-sm-2 <?php echo (count($form['start1']->vars['errors'])) ? ' has-error' : ''; ?>">
                            <?php echo $view['form']->widget($form['start1']); ?>
                            <?php echo $view['form']->errors($form['start1']); ?>
                        </div>
                        <div class="col-sm-2 <?php echo (count($form['stop1']->vars['errors'])) ? ' has-error' : ''; ?>">
                            <?php echo $view['form']->widget($form['stop1']); ?>
                            <?php echo $view['form']->errors($form['stop1']); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2 ">
                            <?php echo $view->render(
                                'MauticCoreBundle:Helper:publishstatus_icon.html.php',
                                ['item' => $item, 'model' => 'deliveryprofile', 'query' => 'bundle='.$bundle]
                            ); ?>
                            <span><?php echo 'Monday'; ?></span>    
                            <br><br>    
                        </div>
                        <div class="col-sm-2 <?php echo (count($form['start2']->vars['errors'])) ? ' has-error' : ''; ?>">
                            <?php echo $view['form']->widget($form['start2']); ?>
                            <?php echo $view['form']->errors($form['start2']); ?>
                        </div>
                        <div class="col-sm-2 <?php echo (count($form['stop2']->vars['errors'])) ? ' has-error' : ''; ?>">
                            <?php echo $view['form']->widget($form['stop2']); ?>
                            <?php echo $view['form']->errors($form['stop2']); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2 ">
                            <?php echo $view->render(
                                'MauticCoreBundle:Helper:publishstatus_icon.html.php',
                                ['item' => $item, 'model' => 'deliveryprofile', 'query' => 'bundle='.$bundle]
                            ); ?>
                            <span><?php echo 'Tuesday'; ?></span>    
                            <br><br>    
                        </div>
                        <div class="col-sm-2 <?php echo (count($form['start3']->vars['errors'])) ? ' has-error' : ''; ?>">
                            <?php echo $view['form']->widget($form['start3']); ?>
                            <?php echo $view['form']->errors($form['start3']); ?>
                        </div>
                        <div class="col-sm-2 <?php echo (count($form['stop3']->vars['errors'])) ? ' has-error' : ''; ?>">
                            <?php echo $view['form']->widget($form['stop3']); ?>
                            <?php echo $view['form']->errors($form['stop3']); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2 ">
                            <?php echo $view->render(
                                'MauticCoreBundle:Helper:publishstatus_icon.html.php',
                                ['item' => $item, 'model' => 'deliveryprofile', 'query' => 'bundle='.$bundle]
                            ); ?>
                            <span><?php echo 'Wednesday'; ?></span>    
                            <br><br>    
                        </div>
                        <div class="col-sm-2 <?php echo (count($form['start4']->vars['errors'])) ? ' has-error' : ''; ?>">
                            <?php echo $view['form']->widget($form['start4']); ?>
                            <?php echo $view['form']->errors($form['start4']); ?>
                        </div>
                        <div class="col-sm-2 <?php echo (count($form['stop4']->vars['errors'])) ? ' has-error' : ''; ?>">
                            <?php echo $view['form']->widget($form['stop4']); ?>
                            <?php echo $view['form']->errors($form['stop4']); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2 ">
                            <?php echo $view->render(
                                'MauticCoreBundle:Helper:publishstatus_icon.html.php',
                                ['item' => $item, 'model' => 'deliveryprofile', 'query' => 'bundle='.$bundle]
                            ); ?>
                            <span><?php echo 'Thursday'; ?></span>    
                            <br><br>    
                        </div>
                        <div class="col-sm-2 <?php echo (count($form['start5']->vars['errors'])) ? ' has-error' : ''; ?>">
                            <?php echo $view['form']->widget($form['start5']); ?>
                            <?php echo $view['form']->errors($form['start5']); ?>
                        </div>
                        <div class="col-sm-2 <?php echo (count($form['stop5']->vars['errors'])) ? ' has-error' : ''; ?>">
                            <?php echo $view['form']->widget($form['stop5']); ?>
                            <?php echo $view['form']->errors($form['stop5']); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2 ">
                            <?php echo $view->render(
                                'MauticCoreBundle:Helper:publishstatus_icon.html.php',
                                ['item' => $item, 'model' => 'deliveryprofile', 'query' => 'bundle='.$bundle]
                            ); ?>
                            <span><?php echo 'Friday'; ?></span>    
                            <br><br>    
                        </div>
                        <div class="col-sm-2 <?php echo (count($form['start6']->vars['errors'])) ? ' has-error' : ''; ?>">
                            <?php echo $view['form']->widget($form['start6']); ?>
                            <?php echo $view['form']->errors($form['start6']); ?>
                        </div>
                        <div class="col-sm-2 <?php echo (count($form['stop6']->vars['errors'])) ? ' has-error' : ''; ?>">
                            <?php echo $view['form']->widget($form['stop6']); ?>
                            <?php echo $view['form']->errors($form['stop6']); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2 ">
                            <?php echo $view->render(
                                'MauticCoreBundle:Helper:publishstatus_icon.html.php',
                                ['item' => $item, 'model' => 'deliveryprofile', 'query' => 'bundle='.$bundle]
                            ); ?>
                            <span><?php echo 'Saturday'; ?></span>    
                            <br><br>    
                        </div>
                        <div class="col-sm-2 <?php echo (count($form['start7']->vars['errors'])) ? ' has-error' : ''; ?>">
                            <?php echo $view['form']->widget($form['start7']); ?>
                            <?php echo $view['form']->errors($form['start7']); ?>
                        </div>
                        <div class="col-sm-2 <?php echo (count($form['stop7']->vars['errors'])) ? ' has-error' : ''; ?>">
                            <?php echo $view['form']->widget($form['stop7']); ?>
                            <?php echo $view['form']->errors($form['stop7']); ?>
                        </div>
                    </div>

                </div>
                </div>
            </div>
            <hr class="mnr-md mnl-md">

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"> Send Delay </h3>
                </div>
                <div class="panel-body">
                <div class="form-group mb-0">
                    <div class="row">
                        <div class="col-sm-6 ">
                            <?php echo $view->render(
                                'MauticCoreBundle:Helper:publishstatus_icon.html.php',
                                ['item' => $item, 'model' => 'deliveryprofile', 'query' => 'bundle='.$bundle]
                            ); ?>
                            <span><?php echo 'Enable Send Delay'; ?></span>    
                            <br><br>    
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 ">
                            <?php echo $view['form']->widget($form['delay_time'], ['attr' => ['placeholder' => $form['delay_time']->vars['label']]]); ?> 
                            <?php echo $view['form']->errors($form['delay_time']); ?>
                        </div>
                        <div class="col-sm-6 ">
                            <label class="control-label mb-xs"><?php echo $view['form']->label($form['delay_time']); ?></label>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            <hr class="mnr-md mnl-md">

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"> Missing Variable Settings </h3>
                </div>
                <div class="panel-body">
                <div class="form-group mb-0">
                    <div class="row">
                        <div class="col-sm-6 ">
                            <?php echo $view->render(
                                'MauticCoreBundle:Helper:publishstatus_icon.html.php',
                                ['item' => $item, 'model' => 'deliveryprofile', 'query' => 'bundle='.$bundle]
                            ); ?>
                            <span><?php echo 'Do Not Send Email if Merge Tags are Missing (e.g. {{First Name}}) '; ?></span>    
                            <br><br>    
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6<?php echo (count($form['missing_notify_email']->vars['errors'])) ? ' has-error' : ''; ?>">
                            <label class="control-label mb-xs"><?php echo $view['form']->label($form['missing_notify_email']); ?></label>
                            <?php echo $view['form']->widget($form['missing_notify_email'], ['attr' => ['placeholder' => $form['missing_notify_email']->vars['label']]]); ?>
                            <?php echo $view['form']->errors($form['missing_notify_email']); ?>
                        </div>
                    </div>
                </div>
                </div>
            </div>
		</div>
	</div>
    <?php echo $view['form']->end($form); ?>
</div>