<?php

/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
if ($tmpl == 'index') {
    $view->extend('MauticDeliveryProfileBundle:DeliveryProfile:index.html.php');
}

$emailserver = [
            'Sendgrid',
            'Amazon SES',
            'Gmail',
            'PHP Mail',
            'Mandrill',
            'Mailjet',
            'Postmark',
            'Elastic Email',
            'Sendmail',
            'Other SMTP Server',
            'Sparkpost',
        ];
$hours = ['12:00 am', '12:30 am', '1:00 am',  '1:30 am', '2:00 am',  '2:30 am', '3:00 am', '3:30 am', '4:30 am', '5:00 am',
                    '5:30 am',  '6:00 am',  '6:30 am',  '7:00 am', '7:30 am',  '8:00 am', '8:30 am', '9:00 am', '9:30 am', '10:00 am',
                    '10:30 am', '11:00 am', '11:30 am', '12:00 pm', '12:30 pm', '1:00 pm', '1:30 pm', '2:00 pm', '2:30 pm', '3:00 pm',
                    '3:30 pm',  '4:00 pm',  '4:30 pm',  '5:00 pm', '5:30 pm',  '6:00 pm', '6:30 pm', '7:00 pm', '7:30 pm', '8:00 pm',
                    '8:30 pm',  '9:00 pm',  '9:30 pm',  '10:00 pm', '10:30 pm', '11:00 pm', '11:30 pm',
                    ];
?>

<?php if (count($items)): ?>
    <div class="table-responsive">
        <table class="table table-hover table-striped table-bordered category-list" id="deliveryprofileTable">
            <thead>
            <tr>
                <?php
                echo $view->render(
                    'MauticCoreBundle:Helper:tableheader.html.php',
                    [
                        'checkall'        => 'true',
                        'target'          => '#deliveryprofileTable',
                        'routeBase'       => 'deliveryprofile',
                        'templateButtons' => [
                            'delete' => $permissions[$permissionBase.':delete'],
                        ],
                        'query' => [
                            'bundle' => $bundle,
                        ],
                    ]
                );

                echo $view->render(
                    'MauticCoreBundle:Helper:tableheader.html.php',
                    [
                        'sessionVar' => 'deliveryprofile',
                        'text'       => '',
                        'class'      => 'col-category-color',
                    ]
                );

                echo $view->render(
                    'MauticCoreBundle:Helper:tableheader.html.php',
                    [
                        'sessionVar' => 'deliveryprofile',
                        'orderBy'    => 'd.mailer_from_name',
                        'text'       => 'Title',
                       // 'class'      => 'col-category-title',
                        'class'   => 'visible-md visible-lg col-page-bundle',
                        'default' => true,
                    ]
                );

                echo $view->render(
                    'MauticCoreBundle:Helper:tableheader.html.php',
                    [
                        'sessionVar' => 'deliveryprofile',
                        'orderBy'    => 'd.mailer_from_email',
                        'text'       => 'Schedule',
                        'class'      => 'visible-md visible-lg col-page-bundle',
                    ]
                );

                echo $view->render(
                    'MauticCoreBundle:Helper:tableheader.html.php',
                    [
                        'sessionVar' => 'deliveryprofile',
                        'orderBy'    => 'd.mailer_return_path',
                        'text'       => 'Delay',
                        'class'      => 'visible-md visible-lg col-page-bundle',
                    ]
                );

                echo $view->render(
                    'MauticCoreBundle:Helper:tableheader.html.php',
                    [
                        'sessionVar' => 'deliveryprofile',
                        'orderBy'    => 'd.id',
                        'text'       => 'Email Server',
                        'class'      => 'visible-md visible-lg col-page-bundle',
                    ]
                );
                ?>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($items as $item): ?>
                <?php
                $list = $item->getScheduleList();
                $val  = unserialize($list);

                $start1 = $val[0][1];
                $item->setStart1($start1);
                $stop1 = $val[0][2];
                $item->setStop1($stop1);

                $start2 = $val[1][1];
                $item->setStart2($start2);
                $stop2 = $val[1][2];
                $item->setStop2($stop2);

                $start3 = $val[2][1];
                $item->setStart3($start3);
                $stop3 = $val[2][2];
                $item->setStop3($stop3);

                $start4 = $val[3][1];
                $item->setStart4($start4);
                $stop4 = $val[3][2];
                $item->setStop4($stop4);

                $start5 = $val[4][1];
                $item->setStart5($start5);
                $stop5 = $val[4][2];
                $item->setStop5($stop5);

                $start6 = $val[5][1];
                $item->setStart6($start6);
                $stop6 = $val[5][2];
                $item->setStop6($stop6);

                $start7 = $val[6][1];
                $item->setStart7($start7);
                $stop7 = $val[6][2];
                $item->setStop7($stop7);

                $schedule = 'Su:'.$hours[$start1].'-'.$hours[$stop1].',Mo:'.$hours[$start2].'-'.$hours[$stop2].',Tu:'.$hours[$start3].'-'.$hours[$stop3].',We:'.$hours[$start4].'-'.$hours[$stop4].',Th:'.$hours[$start5].'-'.$hours[$stop5].',Fr:'.$hours[$start6].'-'.$hours[$stop6].',Sa:'.$hours[$start7].'-'.$hours[$stop7];

                ?>
                <tr>
                    <td>
                        <?php
                        $bundleName = $view['translator']->trans('mautic.'.$item->getBundle().'.'.$item->getBundle());
                        $title      = $view['translator']->trans(
                            'mautic.deliveryprofile.header.edit',
                            ['%type%' => $bundleName, '%name%' => $item->getTitle()]
                        );
                        echo $view->render(
                            'MauticCoreBundle:Helper:list_actions.html.php',
                            [
                                'item'            => $item,
                                'templateButtons' => [
                                    'edit'   => $permissions[$permissionBase.':edit'],
                                    'delete' => $permissions[$permissionBase.':delete'],
                                ],
                                'routeBase' => 'deliveryprofile',
                            ]
                        );
                        ?>
                    </td>
                    <td>
                        <span class="label label-default pa-10" style="background: #<?php echo $item->getColor(); ?>;"> </span>
                    </td>
                    <td>
                        <div>
                            <?php echo $view->render(
                                'MauticCoreBundle:Helper:publishstatus_icon.html.php',
                                ['item' => $item, 'model' => 'deliveryprofile', 'query' => 'bundle='.$bundle]
                            ); ?>
                            <?php if ($permissions[$permissionBase.':edit']): ?>
                                <a href="<?php echo $view['router']->path(
                                    'mautic_deliveryprofile_action',
                                    ['bundle' => $bundle, 'objectAction' => 'edit', 'objectId' => $item->getId()]
                                ); ?>"
                            <?php endif; ?>
                            <span> <?php echo $item->getTitle(); ?> </span>
                            <?php if ($permissions[$permissionBase.':edit']): ?>
                                </a>
                            <?php endif; ?>
                        </div>
                    </td>
                    <td class="visible-md visible-lg">
                        <?php 
                            if (!empty($schedule)) {
                                echo $schedule;
                            }
                        ?>
                    </td>
                    <td class="visible-md visible-lg">
                        <?php echo $item->getDelayTime(); ?>
                    </td>
                    <td class="visible-md visible-lg">
                        <?php 
                                if (!empty($emailserver[$item->getMailerTransport()])) {
                                    echo $emailserver[$item->getMailerTransport()];
                                }
                        ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>

        <div class="panel-footer">
            <?php echo $view->render(
                'MauticCoreBundle:Helper:pagination.html.php',
                [
                    'totalItems' => count($items),
                    'page'       => $page,
                    'limit'      => $limit,
                    'menuLinkId' => 'mautic_deliveryprofile_index',
                    'baseUrl'    => $view['router']->path(
                        'mautic_deliveryprofile_index',
                        [
                            'bundle' => ($bundle == 'deliveryprofile') ? 'all' : $bundle,
                        ]
                    ),
                    'sessionVar' => 'deliveryprofile',
                ]
            ); ?>
        </div>
    </div>
<?php else: ?>
    <?php echo $view->render('MauticCoreBundle:Helper:noresults.html.php', ['tip' => 'mautic.deliveryprofile.noresults.tip']); ?>
<?php endif; ?>
