<?php

/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace Mautic\DeliveryProfileBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Mautic\ApiBundle\Serializer\Driver\ApiMetadataDriver;
use Mautic\CoreBundle\Doctrine\Mapping\ClassMetadataBuilder;
use Mautic\CoreBundle\Entity\FormEntity;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Mapping\ClassMetadata;

/**
 * Class DeliveryProfile.
 */
class DeliveryProfile extends FormEntity
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $color;

    /**
     * @var string
     */
    private $bundle;

/**
 *  New added varialbles.
 */
    // mail
    private $mailer_from_name;

    private $mailer_from_email;

    private $mailer_return_path;

    private $mailer_is_owner;

    private $mailer_transport;

    private $mailer_user;

    private $mailer_password;

    private $mail_handle_method;

    //schedule
    private $schedule_name;

    private $schedule_list = [];

    //delay setting
    private $delay_is_enable;

    private $delay_time;

    //missing
    private $missing_is_enable;

    private $missing_notify_email;

    private $mailer_enable;

    private $start1;
    private $start2;
    private $start3;
    private $start4;
    private $start5;
    private $start6;
    private $start7;

    private $stop1;
    private $stop2;
    private $stop3;
    private $stop4;
    private $stop5;
    private $stop6;
    private $stop7;

    private $check1;

    /**
     * @param ORM\ClassMetadata $metadata
     */
    public static function loadMetadata(ORM\ClassMetadata $metadata)
    {
        $builder = new ClassMetadataBuilder($metadata);

        $builder->setTable('deliveryprofiles')
            ->setCustomRepositoryClass('Mautic\DeliveryProfileBundle\Entity\DeliveryProfileRepository');

        $builder->addId();
        $builder->createField('title', 'string')
                ->length(255)
                ->build();

        $builder->createField('color', 'string')
                ->nullable()
                ->length(7)
                ->build();

        $builder->createField('bundle', 'string')
                ->nullable()
                ->length(50)
                ->build();

        $builder->createField('mailer_from_name', 'string')
                ->length(40)
                ->build();

        $builder->createField('mailer_from_email', 'string')
                ->length(40)
                ->build();

        $builder->createField('mailer_return_path', 'string')
                ->nullable()
                ->length(40)
                ->build();

        $builder->createField('mailer_is_owner', 'boolean')
                ->length(1)
                ->build();

        $builder->createField('mailer_transport', 'string')
                ->length(50)
                ->build();

        $builder->createField('mailer_user', 'string')
                ->nullable()
                ->length(40)
                ->build();

        $builder->createField('mailer_password', 'string')
                ->nullable()
                ->length(64)
                ->build();

        $builder->createField('mail_handle_method', 'string')
                ->length(40)
                ->build();

        $builder->createField('schedule_name', 'string')
                ->length(50)
                ->build();

        $builder->createField('schedule_list', 'string')
                ->nullable()
                ->length(255)
                ->build();

        $builder->createField('delay_is_enable', 'boolean')
                ->length(1)
                ->build();

        $builder->createField('delay_time', 'integer')
                ->nullable()
                ->length(11)
                ->build();

        $builder->createField('missing_is_enable', 'boolean')
                ->length(1)
                ->build();

        $builder->createField('missing_notify_email', 'string')
                ->nullable()
                ->length(40)
                ->build();
    }

    public function getScheduleList()
    {
        return $this->schedule_list;
    }

    public function setScheduleList($list)
    {
        $this->schedule_list = $list;

        return $this;
    }

    public function getMailerEnable()
    {
        return $this->mailer_enable;
    }

    public function setMailerEnable($name)
    {
        $this->mailer_enable = $name;

        return $this;
    }
    /**
     * @param ClassMetadata $metadata
     */
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint(
            'title',
            new NotBlank(
                [
                    'message' => 'mautic.core.title.required',
                ]
            )
        );

        $metadata->addPropertyConstraint(
            'mailer_from_name',
            new NotBlank(
                [
                    'message' => 'Mailer From Name is required.',
                ]
            )
        );

        $metadata->addPropertyConstraint(
            'mailer_from_email',
            new NotBlank(
                [
                    'message' => 'Mailer From Email is required.',
                ]
            )
        );

        $metadata->addPropertyConstraint(
            'schedule_name',
            new NotBlank(
                [
                    'message' => 'Schedule Name is required.',
                ]
            )
        );
    }

    /**
     * Prepares the metadata for API usage.
     *
     * @param $metadata
     */
    public static function loadApiMetadata(ApiMetadataDriver $metadata)
    {
        $metadata->setGroupPrefix('deliveryprofile')
            ->addListProperties(
                [
                    'id',
                    'title',
                    'color',
                ]
            )
            ->addProperties(
                [
                    'bundle',
                    'mailer_from_name',
                    'mailer_from_email',
                    'mailer_return_path',
                ]
            )
            ->build();
    }

    public function __clone()
    {
        $this->id = null;

        parent::__clone();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function setCheck1($check)
    {
        $this->check1 = $check;

        return $this;
    }
    public function getCheck1()
    {
        return $this->check1;
    }

    //start
    public function setStart1($start)
    {
        $this->start1 = $start;

        return $this;
    }
    public function getStart1()
    {
        return $this->start1;
    }

    public function setStart2($start)
    {
        $this->start2 = $start;

        return $this;
    }
    public function getStart2()
    {
        return $this->start2;
    }

    public function setStart3($start)
    {
        $this->start3 = $start;

        return $this;
    }
    public function getStart3()
    {
        return $this->start3;
    }

    public function setStart4($start)
    {
        $this->start4 = $start;

        return $this;
    }
    public function getStart4()
    {
        return $this->start4;
    }

    public function setStart5($start)
    {
        $this->start5 = $start;

        return $this;
    }
    public function getStart5()
    {
        return $this->start5;
    }

    public function setStart6($start)
    {
        $this->start6 = $start;

        return $this;
    }
    public function getStart6()
    {
        return $this->start6;
    }

    public function setStart7($start)
    {
        $this->start7 = $start;

        return $this;
    }
    public function getStart7()
    {
        return $this->start7;
    }

    //stop
    public function setStop1($stop)
    {
        $this->stop1 = $stop;

        return $this;
    }
    public function getStop1()
    {
        return $this->stop1;
    }

    public function setStop2($stop)
    {
        $this->stop2 = $stop;

        return $this;
    }
    public function getStop2()
    {
        return $this->stop2;
    }

    public function setStop3($stop)
    {
        $this->stop3 = $stop;

        return $this;
    }
    public function getStop3()
    {
        return $this->stop3;
    }

    public function setStop4($stop)
    {
        $this->stop4 = $stop;

        return $this;
    }
    public function getStop4()
    {
        return $this->stop4;
    }

    public function setStop5($stop)
    {
        $this->stop5 = $stop;

        return $this;
    }
    public function getStop5()
    {
        return $this->stop5;
    }

    public function setStop6($stop)
    {
        $this->stop6 = $stop;

        return $this;
    }
    public function getStop6()
    {
        return $this->stop6;
    }

    public function setStop7($stop)
    {
        $this->stop7 = $stop;

        return $this;
    }
    public function getStop7()
    {
        return $this->stop7;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return Category
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set color.
     *
     * @param string $color
     *
     * @return Category
     */
    public function setColor($color)
    {
        $this->color = $color;
    }

    /**
     * Get color.
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set bundle.
     *
     * @param string $bundle
     *
     * @return Category
     */
    public function setBundle($bundle)
    {
        $this->bundle = $bundle;
    }

    /**
     * Get bundle.
     *
     * @return string
     */
    public function getBundle()
    {
        return $this->bundle;
    }

    public function getMailerFromName()
    {
        return $this->mailer_from_name;
    }

    public function setMailerFromName($name)
    {
        $this->mailer_from_name = $name;

        return $this;
    }

    public function getMailerFromEmail()
    {
        return $this->mailer_from_email;
    }

    public function setMailerFromEmail($email)
    {
        $this->mailer_from_email = $email;

        return $this;
    }

    public function getMailerReturnPath()
    {
        return $this->mailer_return_path;
    }

    public function setMailerReturnPath($returnPath)
    {
        $this->mailer_return_path = $returnPath;

        return $this;
    }

    public function getMailerIsOwner()
    {
        return $this->mailer_is_owner;
    }

    public function setMailerIsOwner($isowner)
    {
        $this->mailer_is_owner = $isowner;

        return $this;
    }

    public function getMailerTransport()
    {
        return $this->mailer_transport;
    }

    public function setMailerTransport($transport)
    {
        $this->mailer_transport = $transport;

        return $this;
    }

    public function getMailerUser()
    {
        return $this->mailer_user;
    }

    public function setMailerUser($user)
    {
        $this->mailer_user = $user;

        return $this;
    }

    public function getMailerPassword()
    {
        return $this->mailer_password;
    }

    public function setMailerPassword($pass)
    {
        $this->mailer_password = $pass;

        return $this;
    }

    public function getMailHandleMethod()
    {
        return $this->mail_handle_method;
    }

    public function setMailHandleMethod($handle)
    {
        $this->mail_handle_method = $handle;

        return $this;
    }

    /* Schedule Construct*/
    public function getScheduleName()
    {
        return $this->schedule_name;
    }

    public function setScheduleName($name)
    {
        $this->schedule_name = $name;

        return $this;
    }

    public function getDelayIsEnable()
    {
        return $this->delay_is_enable;
    }

    public function setDelayIsEnable($val)
    {
        $this->delay_is_enable = $val;

        return $this;
    }

    public function getDelayTime()
    {
        return $this->delay_time;
    }

    public function setDelayTime($time)
    {
        $this->delay_time = $time;

        return $this;
    }

    public function getMissingIsEnable()
    {
        return $this->missing_is_enable;
    }

    public function setMissingIsEnable($val)
    {
        $this->missing_is_enable = $val;

        return $this;
    }

    public function getMissingNotifyEmail()
    {
        return $this->missing_notify_email;
    }

    public function setMissingNotifyEmail($notify)
    {
        $this->missing_notify_email = $notify;

        return $this;
    }
}
