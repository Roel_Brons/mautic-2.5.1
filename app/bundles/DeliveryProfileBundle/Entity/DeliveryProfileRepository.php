<?php

/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace Mautic\DeliveryProfileBundle\Entity;

use Doctrine\ORM\Tools\Pagination\Paginator;
use Mautic\CoreBundle\Entity\CommonRepository;

/**
 * Class DeliveryProfileRepository.
 */
class DeliveryProfileRepository extends CommonRepository
{
    /**
     * Get a list of entities.
     *
     * @param array $args
     *
     * @return Paginator
     */
    public function getEntities($args = [])
    {
        $q = $this
            ->createQueryBuilder('d')
            ->select('d');

        $args['qb'] = $q;

        return parent::getEntities($args);
    }

    /**
     * @param        $bundle
     * @param string $search
     * @param int    $limit
     * @param int    $start
     *
     * @return array
     */
    public function getDeliveryProfileList($bundle, $search = '', $limit = 10, $start = 0, $includeGlobal = true)
    {
        $q = $this->createQueryBuilder('d');
        $q->select('partial d.{id, title, color, bundle, mailer_from_name, mailer_from_email, mailer_return_path, mailer_is_owner, mailer_transport, mailer_user, mailer_password, mail_handle_method, schedule_name, schedule_list, delay_is_enable, delay_time, missing_is_enable, missing_notify_email}');

        $q->where('d.isPublished = :true')
            ->setParameter('true', true, 'boolean');
/*
        $expr = $q->expr()->orX(
            $q->expr()->eq('d.bundle', ':bundle')
        );

        if ($includeGlobal && 'global' !== $bundle) {
            $expr->add(
                $q->expr()->eq('d.bundle', $q->expr()->literal('global'))
            );
        }

        $q->andWhere($expr)
          ->setParameter('bundle', $bundle);
*/
        if (!empty($search)) {
            $q->andWhere($q->expr()->like('d.title', ':search'))
                ->setParameter('search', "{$search}%");
        }

        $q->orderBy('d.title');

        if (!empty($limit)) {
            $q->setFirstResult($start)
                ->setMaxResults($limit);
        }

        $results = $q->getQuery()->getArrayResult();

        return $results;
    }

    /**
     * @param QueryBuilder $q
     * @param              $filter
     *
     * @return array
     */
    protected function addCatchAllWhereClause(&$q, $filter)
    {
        return $this->addStandardCatchAllWhereClause($q, $filter, [
            'd.title',
//            'd.description',
        ]);
    }

    /**
     * @param QueryBuilder $q
     * @param              $filter
     *
     * @return array
     */
    protected function addSearchCommandWhereClause(&$q, $filter)
    {
        $command                 = $field                 = $filter->command;
        $unique                  = $this->generateRandomParameterName();
        list($expr, $parameters) = parent::addSearchCommandWhereClause($q, $filter);

        switch ($command) {
            case $this->translator->trans('mautic.core.searchcommand.ispublished'):
                $expr                = $q->expr()->eq('d.isPublished', ":$unique");
                $parameters[$unique] = true;
                break;
            case $this->translator->trans('mautic.core.searchcommand.isunpublished'):
                $expr                = $q->expr()->eq('d.isPublished', ":$unique");
                $parameters[$unique] = false;
                break;
        }

        if ($expr && $filter->not) {
            $expr = $q->expr()->not($expr);
        }

        return [
            $expr,
            $parameters,
        ];
    }

    /**
     * @return array
     */
    public function getSearchCommands()
    {
        $commands = [
            'mautic.core.searchcommand.ispublished',
            'mautic.core.searchcommand.isunpublished',
        ];

        return array_merge($commands, parent::getSearchCommands());
    }

    /**
     * @return string
     */
    protected function getDefaultOrder()
    {
        return [
            ['d.title', 'ASC'],
        ];
    }

    /**
     * @param string $bundle
     * @param string $alias
     * @param object $entity
     *
     * @return mixed
     */
    /*
    public function checkUniqueDeliveryProfileAlias($bundle, $alias, $entity = null)
    {
        $q = $this->createQueryBuilder('e')
            ->select('count(e.id) as aliascount')
            ->where('e.alias = :alias')
            ->andWhere('e.bundle = :bundle')
            ->setParameter('alias', $alias)
            ->setParameter('bundle', $bundle);

        if (!empty($entity) && $entity->getId()) {
            $q->andWhere('e.id != :id');
            $q->setParameter('id', $entity->getId());
        }

        $results = $q->getQuery()->getSingleResult();

        return $results['aliascount'];
    }
    */
    /**
     * {@inheritdoc}
     */
    public function getTableAlias()
    {
        return 'd';
    }
}
