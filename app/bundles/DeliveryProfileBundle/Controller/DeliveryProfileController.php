<?php

/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace Mautic\DeliveryProfileBundle\Controller;

use Mautic\CoreBundle\Controller\FormController;
use Mautic\DeliveryProfileBundle\DeliveryProfileEvents;
use Mautic\DeliveryProfileBundle\Event\DeliveryProfileTypesEvent;
use Mautic\DeliveryProfileBundle\Model\DeliveryProfileModel;
use Symfony\Component\HttpFoundation\JsonResponse;

class DeliveryProfileController extends FormController
{
    /**
     * @param        $bundle
     * @param        $objectAction
     * @param int    $objectId
     * @param string $objectModel
     *
     * @return JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function executeDeliveryProfileAction($bundle, $objectAction, $objectId = 0, $objectModel = '')
    {
        if (method_exists($this, "{$objectAction}Action")) {
            return $this->{"{$objectAction}Action"}($bundle, $objectId, $objectModel);
        } else {
            return $this->accessDenied();
        }
    }

    /**
     * @param     $bundle
     * @param int $page
     *
     * @return JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function indexAction($bundle, $page = 1)
    {
        $session = $this->get('session');

        $search = $this->request->get('search', $session->get('mautic.deliveryprofile.filter', ''));
        $bundle = $this->request->get('bundle', $session->get('mautic.deliveryprofile.type', ''));

        if ($bundle) {
            $session->set('mautic.deliveryprofile.type', $bundle);
        }

        // hack to make pagination work for default list view
        if ($bundle == 'all') {
            $bundle = 'deliveryprofile';
        }

        $session->set('mautic.deliveryprofile.filter', $search);

        //set some permissions
        $permissionBase = $this->getModel('deliveryprofile')->getPermissionBase($bundle);
        $permissions    = $this->get('mautic.security')->isGranted(
            [
                $permissionBase.':view',
                $permissionBase.':create',
                $permissionBase.':edit',
                $permissionBase.':delete',
            ],
            'RETURN_ARRAY'
        );

        if (!$permissions[$permissionBase.':view']) {
            return $this->accessDenied();
        }

        if ($this->request->getMethod() == 'POST') {
            $this->setListFilters();
        }

        $viewParams = [
            'page'   => $page,
            'bundle' => $bundle,
        ];

        //set limits
        $limit = $session->get('mautic.deliveryprofile.limit', $this->coreParametersHelper->getParameter('default_pagelimit'));
        $start = ($page === 1) ? 0 : (($page - 1) * $limit);
        if ($start < 0) {
            $start = 0;
        }

        $filter = ['string' => $search];

        if ($bundle != 'deliveryprofile') {
            $filter['force'] = [
                [
                    'column' => 'd.bundle',
                    'expr'   => 'eq',
                    'value'  => $bundle,
                ],
            ];
        }

        $orderBy    = $this->get('session')->get('mautic.deliveryprofile.orderby', 'd.title');
        $orderByDir = $this->get('session')->get('mautic.deliveryprofile.orderbydir', 'DESC');

        $entities = $this->getModel('deliveryprofile')->getEntities(
            [
                'start'      => $start,
                'limit'      => $limit,
                'filter'     => $filter,
                'orderBy'    => $orderBy,
                'orderByDir' => $orderByDir,
            ]
        );

        $count = count($entities);
        if ($count && $count < ($start + 1)) {
            //the number of entities are now less then the current page so redirect to the last page
            if ($count === 1) {
                $lastPage = 1;
            } else {
                $lastPage = (ceil($count / $limit)) ?: 1;
            }
            $viewParams['page'] = $lastPage;
            $session->set('mautic.deliveryprofile.page', $lastPage);
            $returnUrl = $this->generateUrl('mautic_deliveryprofile_index', $viewParams);

            return $this->postActionRedirect(
                [
                    'returnUrl'       => $returnUrl,
                    'viewParameters'  => ['page' => $lastPage],
                    'contentTemplate' => 'MauticDeliveryProfileBundle:DeliveryProfile:index',
                    'passthroughVars' => [
                        //'activeLink'    => '#mautic_deliveryprofile_index',
                        'activeLink'    => '#mautic_'.$bundle.'deliveryprofile_index',
                        'mauticContent' => 'deliveryprofile',
                    ],
                ]
            );
        }

        $DeliveryProfileTypes = ['deliveryprofile' => $this->get('translator')->trans('mautic.core.select')];

        $dispatcher = $this->dispatcher;
        if ($dispatcher->hasListeners(DeliveryProfileEvents::DELIVERY_ON_BUILD_LIST_BUILD)) {
            //die('kill');
            $event = new DeliveryProfileTypesEvent();
            $dispatcher->dispatch(DeliveryProfileEvents::DELIVERY_ON_BUILD_LIST_BUILD, $event);
            $DeliveryProfileTypes = array_merge($DeliveryProfileTypes, $event->getDeliveryProfileTypes());
        }

        //set what page currently on so that we can return here after form submission/cancellation
        $session->set('mautic.deliveryprofile.page', $page);

        $tmpl = $this->request->isXmlHttpRequest() ? $this->request->get('tmpl', 'index') : 'index';

        return $this->delegateView(
            [
                'returnUrl'      => $this->generateUrl('mautic_deliveryprofile_index', $viewParams),
                'viewParameters' => [
                    'bundle'         => $bundle,
                    'permissionBase' => $permissionBase,
                    'searchValue'    => $search,
                    'items'          => $entities,
                    'page'           => $page,
                    'limit'          => $limit,
                    'permissions'    => $permissions,
                    'tmpl'           => $tmpl,
                    'deliveryTypes'  => $DeliveryProfileTypes,
                ],
                'contentTemplate' => 'MauticDeliveryProfileBundle:DeliveryProfile:list.html.php',
                'passthroughVars' => [
                    'activeLink'    => '#mautic_'.$bundle.'deliveryprofile_index',
                    'mauticContent' => 'deliveryprofile',
                    'route'         => $this->generateUrl('mautic_deliveryprofile_index', $viewParams),
                ],
            ]
        );
    }

    /**
     * Generates new form and processes post data.
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction($bundle)
    {
        $session   = $this->get('session'); //session info
        $model     = $this->getModel('deliveryprofile'); //model
        $entity    = $model->getEntity(); //entity info
        $success   = $closeModal   = 0;
        $cancelled = $valid = false;
        $method    = $this->request->getMethod();
        $inForm    = ($method == 'POST') ? $this->request->request->get('deliveryprofile_form[inForm]', 0, true) : $this->request->get('inForm', 0);

        $showSelect = $this->request->get('show_bundle_select', false);

        //not found
        if (!$this->get('mautic.security')->isGranted($model->getPermissionBase($bundle).':create')) {
            return $this->modalAccessDenied();
        }
        //Create the form
        $action = $this->generateUrl('mautic_deliveryprofile_action', [
            'objectAction' => 'new',
            'bundle'       => $bundle,
        ]);

        $form = $model->createForm($entity, $this->get('form.factory'), $action, ['bundle' => $bundle, 'show_bundle_select' => $showSelect]);

        $form['inForm']->setData($inForm);

        ///Check for a submitted form and process it
        if ($method == 'POST') {
            $valid = false;
            if (!$cancelled = $this->isFormCancelled($form)) {
                if ($valid = $this->isFormValid($form)) {
                    $success = 1;

                    //form is valid so process the data
                    $model->saveEntity($entity, $form->get('buttons')->get('save')->isClicked());

                    $this->addFlash('mautic.deliveryprofile.notice.created', [
                        '%name%' => $entity->getName(),
                    ]);
                }
            } else {
                $success = 1;
            }
        }

        $closeModal = ($cancelled || ($valid && $form->get('buttons')->get('save')->isClicked()));

        if ($closeModal) {
            $viewParameters = [
                'page'   => $session->get('mautic.deliveryprofile.page'),
                'bundle' => $bundle,
            ];

            return $this->postActionRedirect([
                'returnUrl'       => $this->generateUrl('mautic_deliveryprofile_index', $viewParameters),
                'viewParameters'  => $viewParameters,
                'contentTemplate' => 'MauticDeliveryProfileBundle:DeliveryProfile:index',
                'passthroughVars' => [
                    'activeLink'    => '#mautic_'.$bundle.'deliveryprofile_index',
                    'mauticContent' => 'deliveryprofile',
                    'closeModal'    => 1,
                ],
            ]);
        } elseif (!empty($valid)) {

            //return edit view to prevent duplicates
            return $this->editAction($bundle, $entity->getId(), true);
        } else {
            return $this->delegateView([
                'returnUrl'      => $this->generateUrl('mautic_deliveryprofile_action', ['objectAction' => 'new', 'bundle' => $bundle]),
                'viewParameters' => [
                    'form'   => $form->createView(),
                    'item'   => $entity,
                    'bundle' => $bundle,
                ],
                'contentTemplate' => 'MauticDeliveryProfileBundle:DeliveryProfile:form.html.php',
                'passthroughVars' => [
                    'activeLink'    => '#mautic_'.$bundle.'deliveryprofile_action',
                    'mauticContent' => 'deliveryprofile',
                    'success'       => $success,
                    'route'         => $this->generateUrl('mautic_deliveryprofile_action', ['objectAction' => 'new', 'bundle' => $bundle]),
                ],
            ]);
        }
    }

    /**
     * Generates edit form and processes post data.
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction($bundle, $objectId, $ignorePost = false)
    {
        // getting the session variables
        $session = $this->get('session');

        /** @var DeliveryProfileModel $model */
        $model = $this->getModel('deliveryprofile');

        // getting the entity for the given objectId
        $entity = $model->getEntity($objectId);

        $success   = $closeModal   = 0;
        $cancelled = $valid = false;
        $method    = $this->request->getMethod();
        $inForm    = ($method == 'POST') ? $this->request->request->get('deliveryprofile_form[inForm]', 0, true) : $this->request->get('inForm', 0);
        //not found
        if ($entity === null) {
            $closeModal = true;
        } elseif (!$this->get('mautic.security')->isGranted($model->getPermissionBase($bundle).':view')) {
            return $this->modalAccessDenied();
        } elseif ($model->isLocked($entity)) {
            return $this->modalAccessDenied();
        }

        //Create the form
        $action = $this->generateUrl(
            'mautic_deliveryprofile_action',
            [
                'objectAction' => 'edit',
                'objectId'     => $objectId,
                'bundle'       => $bundle,
            ]
        );

        $form = $model->createForm($entity, $this->get('form.factory'), $action, ['bundle' => $bundle]);
        $form['inForm']->setData($inForm);

        //Check for a submitted form and process it
        if (!$ignorePost && $method == 'POST') {
            $valid = false;
            if (!$cancelled = $this->isFormCancelled($form)) {
                if ($valid = $this->isFormValid($form)) {
                    $success = 1;

                    //form is valid so process the data
                    $model->saveEntity($entity, $form->get('buttons')->get('save')->isClicked());

                    $this->addFlash(
                        'mautic.deliveryprofile.notice.updated',
                        [
                            '%name%' => $entity->getTitle(),
                        ]
                    );

                    if ($form->get('buttons')->get('apply')->isClicked()) {
                        // Rebuild the form with new action so that apply doesn't keep creating a clone
                        $action = $this->generateUrl(
                            'mautic_deliveryprofile_action',
                            [
                                'objectAction' => 'edit',
                                'objectId'     => $entity->getId(),
                                'bundle'       => $bundle,
                            ]
                        );
                        $form = $model->createForm($entity, $this->get('form.factory'), $action, ['bundle' => $bundle]);
                    }
                }
            } else {
                $success = 1;

                //unlock the entity
                $model->unlockEntity($entity);
            }
        } else {
            //lock the entity
            $model->lockEntity($entity);
        }

        $closeModal = ($closeModal || $cancelled || ($valid && $form->get('buttons')->get('save')->isClicked()));

        if ($closeModal) {
            $viewParameters = [
                'page'   => $session->get('mautic.deliveryprofile.page'),
                'bundle' => $bundle,
            ];

            return $this->postActionRedirect(
                [
                    'returnUrl'       => $this->generateUrl('mautic_deliveryprofile_index', $viewParameters),
                    'viewParameters'  => $viewParameters,
                    'contentTemplate' => 'MauticDeliveryProfileBundle:DeliveryProfile:index',
                    'passthroughVars' => [
                        'activeLink'    => '#mautic_'.$bundle.'deliveryprofile_index',
                        'mauticContent' => 'deliveryprofile',
                        'closeModal'    => 1,
                    ],
                ]
            );
        } else {
            return $this->delegateView(
                [
                    'returnUrl'       => $this->generateUrl('mautic_deliveryprofile_action', ['objectAction' => 'edit', 'objectId' => $objectId, 'bundle' => $bundle]),
                    'contentTemplate' => 'MauticDeliveryProfileBundle:DeliveryProfile:form.html.php',
                    'viewParameters'  => [
                        'form'   => $form->createView(),
                        'item'   => $entity,
                        'bundle' => $bundle,
                    ],
                    'passthroughVars' => [
                        'activeLink'    => '#mautic_'.$bundle.'deliveryprofile_action',
                        'mauticContent' => 'deliveryprofile',
                        'success'       => $success,
                        'route'         => $this->generateUrl('mautic_deliveryprofile_action', ['objectAction' => 'edit', 'objectId' => $objectId, 'bundle' => $bundle]),
                    ],
                ]
            );
        }
    }

    /**
     * Deletes the entity.
     *
     * @param   $objectId
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction($bundle, $objectId)
    {
        $session    = $this->get('session');
        $page       = $session->get('mautic.deliveryprofile.page', 1);
        $viewParams = [
            'page'   => $page,
            'bundle' => $bundle,
        ];
        $returnUrl = $this->generateUrl('mautic_deliveryprofile_index', $viewParams);
        $flashes   = [];

        $postActionVars = [
            'returnUrl'       => $returnUrl,
            'viewParameters'  => $viewParams,
            'contentTemplate' => 'MauticDeliveryProfileBundle:DeliveryProfile:index',
            'passthroughVars' => [
                'activeLink'    => 'mautic_'.$bundle.'deliveryprofile_index',
                'mauticContent' => 'deliveryprofile',
            ],
        ];

        if ($this->request->getMethod() == 'POST') {
            $model  = $this->getModel('deliveryprofile');
            $entity = $model->getEntity($objectId);

            if ($entity === null) {
                $flashes[] = [
                    'type'    => 'error',
                    'msg'     => 'mautic.deliveryprofile.error.notfound',
                    'msgVars' => ['%id%' => $objectId],
                ];
            } elseif (!$this->get('mautic.security')->isGranted($model->getPermissionBase($bundle).':delete')) {
                return $this->accessDenied();
            } elseif ($model->isLocked($entity)) {
                return $this->isLocked($postActionVars, $entity, 'deliveryprofile.deliveryprofile');
            }

            $model->deleteEntity($entity);

            $flashes[] = [
                'type'    => 'notice',
                'msg'     => 'mautic.core.notice.deleted',
                'msgVars' => [
                    '%name%' => $entity->getTitle(),
                    '%id%'   => $objectId,
                ],
            ];
        } //else don't do anything

        return $this->postActionRedirect(
            array_merge($postActionVars, [
                'flashes' => $flashes,
            ])
        );
    }

    /**
     * Deletes a group of entities.
     *
     * @param string $bundle
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function batchDeleteAction($bundle)
    {
        $session    = $this->get('session');
        $page       = $session->get('mautic.deliveryprofile.page', 1);
        $viewParams = [
            'page'   => $page,
            'bundle' => $bundle,
        ];
        $returnUrl = $this->generateUrl('mautic_deliveryprofile_index', $viewParams);
        $flashes   = [];

        $postActionVars = [
            'returnUrl'       => $returnUrl,
            'viewParameters'  => $viewParams,
            'contentTemplate' => 'MauticDeliveryProfileBundle:DeliveryProfile:index',
            'passthroughVars' => [
                'activeLink'    => 'mautic_'.$bundle.'deliveryprofile_index',
                'mauticContent' => 'deliveryprofile',
            ],
        ];

        if ($this->request->getMethod() == 'POST') {
            $model     = $this->getModel('deliveryprofile');
            $ids       = json_decode($this->request->query->get('ids', '{}'));
            $deleteIds = [];

            // Loop over the IDs to perform access checks pre-delete
            foreach ($ids as $objectId) {
                $entity = $model->getEntity($objectId);

                if ($entity === null) {
                    $flashes[] = [
                        'type'    => 'error',
                        'msg'     => 'mautic.deliveryprofile.error.notfound',
                        'msgVars' => ['%id%' => $objectId],
                    ];
                } elseif (!$this->get('mautic.security')->isGranted($model->getPermissionBase($bundle).':delete')) {
                    $flashes[] = $this->accessDenied(true);
                } elseif ($model->isLocked($entity)) {
                    $flashes[] = $this->isLocked($postActionVars, $entity, 'deliveryprofile', true);
                } else {
                    $deleteIds[] = $objectId;
                }
            }

            // Delete everything we are able to
            if (!empty($deleteIds)) {
                $entities = $model->deleteEntities($deleteIds);

                $flashes[] = [
                    'type'    => 'notice',
                    'msg'     => 'mautic.deliveryprofile.notice.batch_deleted',
                    'msgVars' => [
                        '%count%' => count($entities),
                    ],
                ];
            }
        } //else don't do anything

        return $this->postActionRedirect(
            array_merge($postActionVars, [
                'flashes' => $flashes,
            ])
        );
    }
}
