<?php

/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace Mautic\DeliveryProfileBundle\EventListener;

use Mautic\DeliveryProfileBundle\DeliveryProfileEvents;
use Mautic\DeliveryProfileBundle\Event as Events;
use Mautic\DeliveryProfileBundle\Event\DeliveryProfileTypesEvent;
use Mautic\CoreBundle\EventListener\CommonSubscriber;
use Mautic\CoreBundle\Helper\BundleHelper;
use Mautic\CoreBundle\Helper\IpLookupHelper;
use Mautic\CoreBundle\Model\AuditLogModel;

/**
 * Class DeliveryProfileSubscriber.
 */
class DeliveryProfileSubscriber extends CommonSubscriber
{
    /**
     * @var BundleHelper
     */
    protected $bundleHelper;

    /**
     * @var IpLookupHelper
     */
    protected $ipLookupHelper;

    /**
     * @var AuditLogModel
     */
    protected $auditLogModel;

    /**
     * DeliveryProfileSubscriber constructor.
     *
     * @param BundleHelper   $bundleHelper
     * @param IpLookupHelper $ipLookupHelper
     * @param AuditLogModel  $auditLogModel
     */
    public function __construct(BundleHelper $bundleHelper, IpLookupHelper $ipLookupHelper, AuditLogModel $auditLogModel)
    {
        $this->bundleHelper   = $bundleHelper;
        $this->ipLookupHelper = $ipLookupHelper;
        $this->auditLogModel  = $auditLogModel;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            DeliveryProfileEvents::DELIVERY_ON_BUILD_LIST_BUILD => ['onDeliveryProfileBundleListBuild', 0],
            DeliveryProfileEvents::DELIVERYPROFILE_POST_SAVE     => ['onDeliveryProfilePostSave', 0],
            DeliveryProfileEvents::DELIVERY_POST_DELETE          => ['onDeliveryProfileDelete', 0],
        ];
    }

    /**
     * Add bundle to the deliveryprofile.
     *
     * @param DeliveryProfileTypesEvent $event
     */
    public function onDeliveryProfileBundleListBuild(DeliveryProfileTypesEvent $event)
    {
        $bundles = $this->bundleHelper->getMauticBundles(true);

        foreach ($bundles as $bundle) {
            if (!empty($bundle['config']['deliveryprofiles'])) {
                foreach ($bundle['config']['deliveryprofiles'] as $type => $label) {
                    $event->addDeliveryProfileType($type, $label);
                }
            }
        }
    }

    /**
     * Add an entry to the audit log.
     *
     * @param Events\DeliveryProfileEvent $event
     */
    public function onDeliveryProfilePostSave(Events\DeliveryProfileEvent $event)
    {
        $deliveryprofile = $event->getDeliveryProfile();
        if ($details = $event->getChanges()) {
            $log = [
                'bundle'    => 'deliveryprofile',
                'object'    => 'deliveryprofile',
                'objectId'  => $deliveryprofile->getId(),
                'action'    => ($event->isNew()) ? 'create' : 'update',
                'details'   => $details,
                'ipAddress' => $this->ipLookupHelper->getIpAddressFromRequest(),
            ];
            $this->auditLogModel->writeToLog($log);
        }
    }

    /**
     * Add a delete entry to the audit log.
     *
     * @param Events\DeliveryProfileEvent $event
     */
    public function onDeliveryProfileDelete(Events\DeliveryProfileEvent $event)
    {
        $deliveryprofile = $event->getDeliveryProfile();
        $log      = [
            'bundle'    => 'deliveryprofile',
            'object'    => 'deliveryprofile',
            'objectId'  => $deliveryprofile->deletedId,
            'action'    => 'delete',
            'details'   => ['name' => $deliveryprofile->getTitle()],
            'ipAddress' => $this->ipLookupHelper->getIpAddressFromRequest(),
        ];
        $this->auditLogModel->writeToLog($log);
    }
}
