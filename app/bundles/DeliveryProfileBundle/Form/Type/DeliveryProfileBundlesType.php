<?php

/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace Mautic\DeliveryProfileBundle\Form\Type;

use Mautic\DeliveryProfileBundle\DeliveryProfileEvents;
use Mautic\DeliveryProfileBundle\Event\DeliveryProfileTypesEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DeliveryProfileBundlesType.
 */
class DeliveryProfileBundlesType extends AbstractType
{
    private $dispatcher;

    /**
     * DeliveryProfileBundlesType constructor.
     *
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(EventDispatcherInterface $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'choices' => function (Options $options) {
                if ($this->dispatcher->hasListeners(DeliveryProfileEvents::DELIVERY_ON_BUILD_LIST_BUILD)) {
                    $event = $this->dispatcher->dispatch(DeliveryProfileEvents::DELIVERY_ON_BUILD_LIST_BUILD, new DeliveryProfileTypesEvent());
                    $types = $event->getDeliveryProfileTypes();
                } else {
                    $types = [];
                }

                return $types;
            },
            'expanded' => false,
            'multiple' => false,
            'required' => false,
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'deliveryprofile_bundles_form';
    }

    public function getParent()
    {
        return 'choice';
    }
}
