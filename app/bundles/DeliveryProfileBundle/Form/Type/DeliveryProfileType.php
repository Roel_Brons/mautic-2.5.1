<?php

/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace Mautic\DeliveryProfileBundle\Form\Type;

use Mautic\CoreBundle\Form\EventListener\CleanFormSubscriber;
use Mautic\CoreBundle\Form\EventListener\FormExitSubscriber;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class DeliveryProfileType.
 */
class DeliveryProfileType extends AbstractType
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var Session
     */
    private $session;

    /**
     * DeliveryProfileType constructor.
     *
     * @param TranslatorInterface $translator
     * @param Session             $session
     */
    public function __construct(TranslatorInterface $translator, Session $session)
    {
        $this->translator = $translator;
        $this->session    = $session;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventSubscriber(new CleanFormSubscriber());
        $builder->addEventSubscriber(new FormExitSubscriber('deliveryprofile.deliveryprofile', $options));

        if (!$options['data']->getId()) {
            // Do not allow custom bundle
            if ($options['show_bundle_select'] == true) {
                // Create new category from category bundle - let user select the bundle

                $selected = $this->session->get('mautic.deliveryprofile.type', 'deliveryprofile');
                $builder->add(
                    'bundle',
                    'deliveryprofile_bundles_form',
                    [
                        'label'      => 'mautic.core.type',
                        'label_attr' => ['class' => 'control-label'],
                        'attr'       => ['class' => 'form-control'],
                        'required'   => true,
                        'data'       => $selected,
                    ]
                );
            } else {
                // Create new category directly from another bundle - preset bundle

                $builder->add(
                    'bundle',
                    'hidden',
                    [
                        'data' => $options['bundle'],
                    ]
                );
            }
        }

        $builder->add(
            'title',
            'text',
            [
                'label'      => 'mautic.core.title',
                'label_attr' => ['class' => 'control-label'],
                'attr'       => ['class' => 'form-control'],
                //'data'       => 'testval',
            ]
        );

        $builder->add(
            'color',
            'text',
            [
                'label'      => 'mautic.core.color',
                'label_attr' => ['class' => 'control-label'],
                'attr'       => [
                    'class'       => 'form-control',
                    'data-toggle' => 'color',
                ],
                'required' => false,
            ]
        );

        $builder->add('isPublished', 'yesno_button_group');

        $builder->add(
            'mailer_from_name',
            'text',
            [
                'label'      => 'Name to send mail as',
                'label_attr' => ['class' => 'control-label'],
                'attr'       => ['class' => 'form-control',
                'tooltip'                => 'mautic.email.config.mailer.from.name.tooltip', ],
            ]
        );

        $builder->add(
            'mailer_from_email',
            'text',
            [
                'label'      => 'E-mail address to send mail from',
                'label_attr' => ['class' => 'control-label'],
                'attr'       => [
                'class'   => 'form-control',
                'tooltip' => 'mautic.email.config.mailer.from.email.tooltip', ],
            ]
        );

        $builder->add(
            'mailer_return_path',
            'text',
            [
                'label'      => 'Custom return path(bounce) address',
                'label_attr' => ['class' => 'control-label'],
                'attr'       => [
                'class'   => 'form-control',
                'tooltip' => 'mautic.email.config.mailer.return.path.tooltip', ],
                'required' => false,
            ]
        );

        $builder->add(
            'mailer_is_owner',
            'yesno_button_group',
            [
                'label'      => 'Mailer is owner',
                'label_attr' => ['class' => 'control-label'],
                'attr'       => ['class' => 'form-control'],
                'data'       => true,
                'required'   => true,
            ]
        );

        $transports = [
            'Sendgrid',
            'Amazon SES',
            'Gmail',
            'PHP Mail',
            'Mandrill',
            'Mailjet',
            'Postmark',
            'Elastic Email',
            'Sendmail',
            'Other SMTP Server',
            'Sparkpost',
        ];
        $handle_method = [
            'Send immediately',
            'Queue',
        ];

        $hours = ['12:00 am', '12:30 am', '1:00 am',  '1:30 am', '2:00 am',  '2:30 am', '3:00 am', '3:30 am', '4:00 am', '4:30 am', '5:00 am', '5:30 am',  '6:00 am',  '6:30 am',  '7:00 am', '7:30 am',  '8:00 am', '8:30 am', '9:00 am', '9:30 am', '10:00 am', '10:30 am', '11:00 am', '11:30 am', '12:00 pm', '12:30 pm', '1:00 pm', '1:30 pm', '2:00 pm', '2:30 pm', '3:00 pm',
            '3:30 pm',  '4:00 pm',  '4:30 pm',  '5:00 pm', '5:30 pm',  '6:00 pm', '6:30 pm', '7:00 pm', '7:30 pm', '8:00 pm', '8:30 pm',  '9:00 pm',  '9:30 pm',  '10:00 pm', '10:30 pm', '11:00 pm', '11:30 pm',
                ];

        $builder->add(
            'mailer_transport',
            'choice',
                [
                    'choices'     => $transports,
                    'label'       => 'Service to send mail through',
                    'label_attr'  => ['class' => 'control-label'],
                    'attr'        => ['class' => 'form-control'],
                    'multiple'    => false,
                    'required'    => true,
                    'empty_value' => false,
                ]
        );

        $builder->add(
            'mailer_user',
            'text',
            [
                'label'      => 'Username for the selected mail service',
                'label_attr' => ['class' => 'control-label'],
                'attr'       => [
                'class'   => 'form-control',
                'tooltip' => 'mautic.email.config.mailer.user.tooltip',
                ],
                'required' => false,
            ]
        );

        $builder->add(
            'mailer_password',
            'text',
            [
                'label'      => 'Password for the selected mail service',
                'label_attr' => ['class' => 'control-label'],
                'attr'       => [
                'class'        => 'form-control',
                'placeholder'  => 'mautic.user.user.form.passwordplaceholder',
                    'preaddon' => 'fa fa-lock',
                    'tooltip'  => 'mautic.email.config.mailer.password.tooltip',
                    ],
                'required' => false,
            ]
        );

        $builder->add(
            'mail_handle_method',
            'choice',
                [
                    'choices'    => $handle_method,
                    'label'      => 'How should email be handled?',
                    'label_attr' => ['class' => 'control-label'],
                    'attr'       => [
                    'class'   => 'form-control',
                    'tooltip' => 'mautic.email.config.mailer.spool.type.tooltip',
                    ],
                    'multiple'    => false,
                    'required'    => true,
                    'empty_value' => false,
                ]
        );

        $builder->add(
            'mailer_test_connection_button',
            'standalone_button',
            [
                'label'    => 'mautic.email.config.mailer.transport.test_connection',
                'required' => false,
                'attr'     => [
                    'class' => 'btn btn-success',
                //    'onclick' => 'Mautic.testEmailServerConnection()',
                ],
            ]
        );

        $builder->add(
            'mailer_test_send_button',
            'standalone_button',
            [
                'label'    => 'mautic.email.config.mailer.transport.test_send',
                'required' => false,
                'attr'     => [
                    'class' => 'btn btn-info',
                //    'onclick' => 'Mautic.testEmailServerConnection(true)',
                ],
            ]
        );

        $builder->add(
            'schedule_name',
            'text',
            [
                'label'      => 'Delivery Schedule Name',
                'label_attr' => ['class' => 'control-label'],
                'attr'       => ['class' => 'form-control'],
                'required'   => true,
            ]
        );

        $builder->add(
            'delay_time',
            'integer',
            [
                'label'      => 'Seconds between Emails',
                'label_attr' => ['class' => 'control-label'],
                'attr'       => ['class' => 'form-control'],
                'required'   => false,
            ]
        );

        $builder->add(
            'missing_notify_email',
            'text',
            [
                'label'      => 'Notification Email',
                'label_attr' => ['class' => 'control-label'],
                'attr'       => ['class' => 'form-control'],
                'required'   => false,
            ]
        );

        $builder->add(
            'inForm',
            'hidden',
            [
                'mapped' => false,
            ]
        );

        $builder->add(
            'start1',
            'choice',
                [
                    'choices'     => $hours,
                    'label'       => 'Sunday',
                    'label_attr'  => ['class' => 'control-label'],
                    'attr'        => ['class' => 'form-control'],
                    'multiple'    => false,
                    'required'    => true,
                    'empty_value' => false,
                ]
        );
        $builder->add(
            'start2',
            'choice',
                [
                    'choices'     => $hours,
                    'label'       => 'Monday',
                    'label_attr'  => ['class' => 'control-label'],
                    'attr'        => ['class' => 'form-control'],
                    'multiple'    => false,
                    'required'    => true,
                    'empty_value' => false,
                ]
        );
        $builder->add(
            'start3',
            'choice',
                [
                    'choices'     => $hours,
                    'label'       => 'Tuesday',
                    'label_attr'  => ['class' => 'control-label'],
                    'attr'        => ['class' => 'form-control'],
                    'multiple'    => false,
                    'required'    => true,
                    'empty_value' => false,
                ]
        );
        $builder->add(
            'start4',
            'choice',
                [
                    'choices'     => $hours,
                    'label'       => 'Wednesday',
                    'label_attr'  => ['class' => 'control-label'],
                    'attr'        => ['class' => 'form-control'],
                    'multiple'    => false,
                    'required'    => true,
                    'empty_value' => false,
                ]
        );
        $builder->add(
            'start5',
            'choice',
                [
                    'choices'     => $hours,
                    'label'       => 'Thursday',
                    'label_attr'  => ['class' => 'control-label'],
                    'attr'        => ['class' => 'form-control'],
                    'multiple'    => false,
                    'required'    => true,
                    'empty_value' => false,
                ]
        );
        $builder->add(
            'start6',
            'choice',
                [
                    'choices'     => $hours,
                    'label'       => 'Friday',
                    'label_attr'  => ['class' => 'control-label'],
                    'attr'        => ['class' => 'form-control'],
                    'multiple'    => false,
                    'required'    => true,
                    'empty_value' => false,
                ]
        );
        $builder->add(
            'start7',
            'choice',
                [
                    'choices'     => $hours,
                    'label'       => 'Saturday',
                    'label_attr'  => ['class' => 'control-label'],
                    'attr'        => ['class' => 'form-control'],
                    'multiple'    => false,
                    'required'    => true,
                    'empty_value' => false,
                ]
        );

        $builder->add(
            'stop1',
            'choice',
                [
                    'choices'     => $hours,
                    'label'       => 'Sunday',
                    'label_attr'  => ['class' => 'control-label'],
                    'attr'        => ['class' => 'form-control'],
                    'multiple'    => false,
                    'required'    => true,
                    'empty_value' => false,
                ]
        );
        $builder->add(
            'stop2',
            'choice',
                [
                    'choices'     => $hours,
                    'label'       => 'Monday',
                    'label_attr'  => ['class' => 'control-label'],
                    'attr'        => ['class' => 'form-control'],
                    'multiple'    => false,
                    'required'    => true,
                    'empty_value' => false,
                ]
        );
        $builder->add(
            'stop3',
            'choice',
                [
                    'choices'     => $hours,
                    'label'       => 'Tuesday',
                    'label_attr'  => ['class' => 'control-label'],
                    'attr'        => ['class' => 'form-control'],
                    'multiple'    => false,
                    'required'    => true,
                    'empty_value' => false,
                ]
        );
        $builder->add(
            'stop4',
            'choice',
                [
                    'choices'     => $hours,
                    'label'       => 'Wednesday',
                    'label_attr'  => ['class' => 'control-label'],
                    'attr'        => ['class' => 'form-control'],
                    'multiple'    => false,
                    'required'    => true,
                    'empty_value' => false,
                ]
        );
        $builder->add(
            'stop5',
            'choice',
                [
                    'choices'     => $hours,
                    'label'       => 'Thursday',
                    'label_attr'  => ['class' => 'control-label'],
                    'attr'        => ['class' => 'form-control'],
                    'multiple'    => false,
                    'required'    => true,
                    'empty_value' => false,
                ]
        );
        $builder->add(
            'stop6',
            'choice',
                [
                    'choices'     => $hours,
                    'label'       => 'Friday',
                    'label_attr'  => ['class' => 'control-label'],
                    'attr'        => ['class' => 'form-control'],
                    'multiple'    => false,
                    'required'    => true,
                    'empty_value' => false,
                ]
        );
        $builder->add(
            'stop7',
            'choice',
                [
                    'choices'     => $hours,
                    'label'       => 'Saturday',
                    'label_attr'  => ['class' => 'control-label'],
                    'attr'        => ['class' => 'form-control'],
                    'multiple'    => false,
                    'required'    => true,
                    'empty_value' => false,
                ]
        );

        $builder->add('buttons', 'form_buttons');

        if (!empty($options['action'])) {
            $builder->setAction($options['action']);
        }
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class'         => 'Mautic\DeliveryProfileBundle\Entity\DeliveryProfile',
                'show_bundle_select' => false,
                'bundle'             => function (Options $options) {
                    if (!$bundle = $options['data']->getBundle()) {
                        $bundle = 'delveryprofile';
                    }

                    return $bundle;
                },
            ]
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'deliveryprofile_form';
    }
}
