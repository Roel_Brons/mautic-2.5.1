<?php

/*
 * @copyright   2014 Mautic Contributors. All rights reserved
 * @author      Mautic
 *
 * @link        http://mautic.org
 *
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace Mautic\DeliveryProfileBundle\Form\Type;

use Doctrine\ORM\EntityManager;
use Mautic\CoreBundle\Form\DataTransformer\IdToEntityModelTransformer;
use Mautic\DeliveryProfileBundle\Model\DeliveryProfileModel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\Router;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class DeliveryProfileListType.
 */
class DeliveryProfileListType extends AbstractType
{
    private $em;

    private $model;

    private $translator;

    private $router;

    /**
     * DeliveryProfileListType constructor.
     *
     * @param EntityManager        $em
     * @param TranslatorInterface  $translator
     * @param DeliveryProfileModel $model
     * @param Router               $router
     */
    public function __construct(EntityManager $em, TranslatorInterface $translator, DeliveryProfileModel $model, Router $router)
    {
        $this->em         = $em;
        $this->translator = $translator;
        $this->model      = $model;
        $this->router     = $router;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $transformer = new IdToEntityModelTransformer($this->em, 'MauticDeliveryProfileBundle:DeliveryProfile', 'id');
        $builder->addModelTransformer($transformer);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'choices' => function (Options $options) {
                $createNew = $this->translator->trans('mautic.deliveryprofile.createnew');
                $deliveryprofiles = $this->model->getLookupResults($options['bundle'], '', 0);
                $choices = [];
                foreach ($deliveryprofiles as $l) {
                    $choices[$l['id']] = $l['title'];
                }
                $choices['new'] = $createNew;

                return $choices;
            },
            'label'       => 'mautic.core.category',
            'label_attr'  => ['class' => 'control-label'],
            'multiple'    => false,
            'empty_value' => 'mautic.core.form.uncategorized',
            'attr'        => function (Options $options) {
                $modalHeader = $this->translator->trans('mautic.deliveryprofile.header.new');
                $newUrl = $this->router->generate('mautic_deliveryprofile_action', [
                    'objectAction' => 'new',
                    'bundle'       => $options['bundle'],
                    'inForm'       => 1,
                ]);

                return [
                    'class'    => 'form-control category-select',
                    'onchange' => "Mautic.loadAjaxModalBySelectValue(this, 'new', '{$newUrl}', '{$modalHeader}');",
                ];
            },
            'required' => false,
        ]);

        $resolver->setRequired(['bundle']);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'deliveryprofile';
    }

    /**
     * @return string
     */
    public function getParent()
    {
        return 'choice';
    }
}
