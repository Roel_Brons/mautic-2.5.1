/** CategoryBundle **/

Mautic.deliveryprofileOnLoad = function (container, response) {
    //alert('TEST');
    if (mQuery(container + ' #list-search').length) {
        Mautic.activateSearchAutocomplete('list-search', 'deliveryprofile');
    }

    if (response && response.inForm) {
        var newOption = mQuery('<option />').val(response.categoryId);
        newOption.html(response.categoryName);

        mQuery(".category-select option:last").prev().before(newOption);
        newOption.prop('selected', true);

        mQuery('.category-select').trigger("chosen:updated");
    }
};
